# 模版广告(MSPrerenderAd)

使用说明：通过设置MSPrerenderAd代理，获取广告、展示、点击、关闭等回调。值得注意的是一定要设置controller，即跳转落地页需要的viewController。

**注意事项**

**1、如果使用同一个广告请求对象加载多次广告，请将上次请求到的广告移除后再展示本次请求回来的广告。**

**2、请在加载模版广告前进行个性化参数设置**

**3、如模版广告渲染失败即（收到msPrerenderRenderError渲染失败回调后），请将该模版广告从当前展示列表中移除**

#### 1.加载模版广告

引入头文件：

```objc
#import <MSAdSDK/MSPrerenderAd.h>
```

您需要确认您遵循了**MSPrerenderAdDelegate**代理协议：

```objc
@interface MSPrerenderAdViewController ()<MSPrerenderAdDelegate>

@end
```

初始化模版广告请求实例

```objc
MSPrerenderAd *  prerenderAd = [[MSPrerenderAd alloc]initWithCurController:self];
prerenderAd.delegate = self;
```

媒体方如果接入了广点通模版2.0，请按如下设置：

```objc
prerenderAd.prerenderType = MSGDTPrerender2_0Version;//默认加载广点通模版
```

加载模版广告前务必传入广告位ID

```objc
[prerenderAd loadAd:pid];
```

#### 2. 实现MSPrerenderAd的Delegate

您可以实现**MSPrerenderAd Delegate**的方法来获取模版广告的各种事件：

```objc
/**
 *  请求广告条数据成功后调用
 *  详解:当接收服务器返回的广告数据成功后调用该函数
 */
- (void)msPrerenderLoaded:(MSPrerenderAd *)prerenderAd adViewArray:(NSArray<UIView*>*)adViewArray;
/**
 *  请求广告条数据失败后调用
 *  详解:当接收服务器返回的广告数据失败后调用该函数
 */
- (void)msPrerenderError:(MSPrerenderAd *)prerenderAd error:(NSError *)error;
/**
*  请求平台广告条数据失败后调用
*  详解:当接收服务器返回的广告数据失败后调用该函数
*/
- (void)msPrerenderPlatformError:(MSPlatform)platform ad:(MSPrerenderAd *)prerenderAd error:(NSError *)error;
/**
 *  广告视图渲染成功
 *  详解:广告视图渲染成功后调用该函数
 */
- (void)msPrerenderRenderSuccess:(UIView *)adView;
/**
 *  广告视图渲染失败
 *  详解:广告视图渲染失败后调用该函数
 */
- (void)msPrerenderRenderError:(UIView *)adView error:(NSError *)error;
/**
 *  prerender广告曝光
 */
- (void)msPrerenderShow:(UIView *)adView;
/**
 *  prerender点击回调
 */
- (void)msPrerenderClicked:(UIView *)adView;
/**
 *  prerender条被用户关闭时调用
 *  详解:用户有可能点击关闭按钮从而把广告条关闭
 */
- (void)msPrerenderClosed:(UIView *)adView;
/**
 获取当前广告的行业ID、创意ID、广告主ID
 @param adCats 投放行业ID
 @param adCids 创意ID
 @param aderIds 广告主ID
 提示：请求MS平台广告时触发该回调
 */
- (void)msPrerenderAdCats:(NSArray<NSString *> *)adCats
                    AdCid:(NSArray<NSString *> *)adCids
                  AderIds:(NSArray<NSString *> *)aderIds;
/**
 获取ecpm参数
 详解：此回调只有在MS平台广告加载成功后才会触发
 */
- (void)msPrerenderEcpm:(NSString *)ecpm;
```

#### 3.获取当前展示的广告归属平台

请在此回调中获取广告归属，请参考[平台对应关系](SDK接入配置)。

```objc
- (void)msPrerenderLoaded:(MSPrerenderAd *)prerenderAd adViewArray:(NSArray<UIView*>*)adViewArray{
		NSLog(@"%d",prerenderAd.platform);
}
```

#### 4.特殊回调说明

| 广告回调                 | 回调说明                                                     |
| ------------------------ | ------------------------------------------------------------ |
| msPrerenderError         | 在该广告位上配置的多家平台依次调度仍未加载到广告后调用，此回调代表本次请求动作已结束 |
| msPrerenderPlatformError | 当前广告平台请求失败后调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败后触发该回调，注意此回调并不代表本次请求动作已经结束且可能存在多次回调的情况 |