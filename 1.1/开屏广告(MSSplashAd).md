# 开屏广告(MSSplashAd)

类型说明：开屏广告主要是 APP 启动时展示的全屏广告视图，开发者只要按照规范接入就能够展示设计好的视图。

**注意事项**

**1、接入开屏广告时，确保keywindow的rootViewController没有弹出其他控制器，否则开屏广告无法展示**

**2、如果开屏广告调用加载并展示接口即loadAndShowSplashAd接口，不需要再次调用show接口**

**3、如果开屏广告是加载和展示分离操作，请在收到开屏广告展示准备就绪回调后，调用show接口**

**4、其他开屏广告个性化参数设置务必在加载广告前传入**

#### 1.加载开屏广告

引入头文件：

```objc
#import <MSAdSDK/MSSplashAd.h>
```

您需要确认您遵循了**MSSplashAdDelegate**代理协议：

```objc
@interface MSSplashAdViewController ()<MSSplashAdDelegate>

@end
```

初始化开屏广告请求实例

```objc
self.splash = [[MSSplashAd alloc]init];
self.splash.delegate = self;
```

加载开屏广告前务必传入广告位ID

```objc
[self.splash loadSplashAd:pid];
```

#### 2. 展示开屏广告

为了保证广告正常展示，建议在收到开屏广告展示准备就绪回调后，调用此接口

```objc
- (void)msSplashAdReadySuccess:(MSSplashAd *)splashAd{
    [self.splash showSplashAd];
}
```

#### 3. 实现MSSplashAd的Delegate

您可以实现**MSSplashAd Delegate**的方法来获取开屏广告的各种事件：

```objc
/**
 * 开始加载广告
 */
- (void)msSplashStartLoaded:(MSSplashAd *)splashAd;
/**
 * 开屏广告加载成功
 */
- (void)msSplashLoaded:(MSSplashAd *)splashAd;
/**
 开屏广告展示准备就绪，在此回调中调用show接口
 注意⚠️：如果调用的是loadAndShowSplashAd接口则不需要调用show接口
 */
- (void)msSplashAdReadySuccess:(MSSplashAd *)splashAd;
/**
 * 开屏广告投屏成功
 */
- (void)msSplashPresent:(MSSplashAd *)splashAd;
/**
 *  开屏广告曝光成功
 */
- (void)msSplashShow:(MSSplashAd *)splashAd;
/**
 *  开屏广告曝光失败
 *  详解：可能广告素材异常或三方异常导致无法广告曝光
 */
- (void)msSplashAdShowFail:(MSSplashAd *)splashAd error:(NSError *)error;
/**
 *  开屏广告失败（最终请求失败）
 */
- (void)msSplashError:(MSSplashAd *)splashAd withError:(NSError *)error;
/**
*  平台Error（当前平台请求失败）
*/
- (void)msSplashPlatformError:(MSPlatform)platform
                     splashAd:(MSSplashAd *)splashAd
                        error:(NSError*)error;
/**
 *  开屏广告点击回调
 */
- (void)msSplashClicked:(MSSplashAd *)splashAd;
/**
 *  开屏广告将要关闭回调
 */
- (void)msSplashWillClosed:(MSSplashAd *)splashAd;
/**
 *  开屏广告关闭回调
 */
- (void)msSplashClosed:(MSSplashAd *)splashAd;
/**
 *  点击以后全屏广告页已经关闭
 */
- (void)msSplashDetailClosed:(MSSplashAd *)splashAd;
/**
 * 当用户点击跳过按钮时触发
 */
- (void)msSplashSkip:(MSSplashAd *)splashAd;
/**
 获取当前广告的行业ID、创意ID、广告主ID
 @param adCats 投放行业ID
 @param adCids 创意ID
 @param aderIds 广告主ID
 提示：请求MS平台广告时触发该回调
 */
- (void)msSplashAdCats:(NSArray<NSString *> *)adCats
                 AdCid:(NSArray<NSString *> *)adCids
               AderIds:(NSArray<NSString *> *)aderIds;
/**
 获取ecpm参数
 详解：此回调只有在MS平台广告加载成功后才会触发
 */
- (void)msSplashEcpm:(NSString *)ecpm;
```

#### 4.获取当前展示的广告归属平台

请在此回调中获取广告归属，请参考[平台对应关系](SDK接入配置)。

```objc
- (void)msSplashLoaded:(MSSplashAd *)splashAd{
		NSLog(@"%d",splashAd.platform);
}
```

#### 5.特殊回调说明

| 广告回调              | 回调说明                                                     |
| --------------------- | ------------------------------------------------------------ |
| msSplashStartLoaded   | 当广告平台加载广告时调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败会自动切换备选平台并再次触发该回调 |
| msSplashError         | 在该广告位上配置的多家平台依次调度仍未加载到广告后调用，此回调代表本次请求动作已结束 |
| msSplashPlatformError | 当前广告平台请求失败后调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败后触发该回调，注意此回调并不代表本次请求动作已经结束且可能存在多次回调的情况 |
| msSplashSkip          | 当用户点击跳过按钮时触发，第三方广告平台不存在该回调则不会触发 |

#### 6、开屏广告其他接口用法说明

##### 6.1 开屏广告已按照工信部要求完成适配，提供部分区域可点击接口，媒体方可通过该属性splashClickText进行配置，只可配置MS平台，配置如下：

```objc
self.splash.splashClickText = @"点击前往第三方页面 >";//务必在加载广告前配置，可自定义文案
```

##### 6.2 配置自定义跳过按钮，可通过skipView属性配置，目前MS、广点通支持该配置方式，示例如下：

```objc
self.splash.skipView = self.customSkipView;//加载广告前配置，指定跳过按钮
```

##### 6.3 配置自定义底部视图，可通过bottomView属性配置，目前MS、广点通、穿山甲支持该配置，示例如下：

```objc
self.splash.bottomView = [self bottomView];//加载广告前配置，
```

##### 6.4 获取MS平台返回的开屏素材尺寸，可通过msSplashAdSize属性获取，其他平台返回为0，需在开屏广告展示准备就绪回调中获取，示例如下：

```objc
- (void)msSplashAdReadySuccess:(MSSplashAd *)splashAd {
  //width & height 代表像素值
    NSLog(@"%@",[NSString stringWithFormat:@"===========width=%lfheight-%lf",splashAd.splashSize.width,splashAd.splashSize.height]);
}
```

##### 6.5 获取开屏广告有两种加载方式：

方式一、加载成功并立即展示

```objc
- (void)viewDidLoad{
		[self.splash loadAndShowSplashAd:pid];
  //或指定window方式
  //[self loadAndShowSplashAd:pid inWindow:window];
}
```

方式二、加载和展示接口分别调用

```objc
- (void)viewDidLoad{
		[self.splash loadSplashAd:pid];//调用加载方法
}
//收到开屏广告展示准备就绪回调
- (void)msSplashAdReadySuccess:(MSSplashAd *)splashAd{
  	[self.splash showSplashAd];
}
```

**6.6 设置MS平台开屏页是否隐藏系统状态栏**(默认展示)

```
self.splash.hideSplashStatusBar = YES;
```

**6.7 点睛功能介绍**

**注意点**

**开屏广告请求对象（即当前请求开屏广告MSSplashAd实例）不要过早释放，否则影响广告相关交互逻辑**

1、如何获取点睛视图，请在MSSplashAd声明文件中查看该属性

```
/**
获取开屏点睛view
注意：
 1、收到msSplashAdReadySuccess回调后获取开屏点睛view
 2、推荐尺寸比例为9:16，最小尺寸推荐为126*224
 3、切记⚠️⚠️⚠️不要在splashZoomView添加UITapGestureRecognizer手势，否则会阻断事件响应链广告无法点击
 */
@property(nonatomic,strong,readonly)UIView *splashZoomView;
/**
 设置点睛VC
 注意：
 1、请在展示出开屏广告后设置VC，⚠️传入的VC不能present其他VC，否则页面无法跳转
 */
@property(nonatomic,weak)UIViewController *splashZoomViewController;
```

2、如何使用 (建议在收到开屏广告关闭回调中处理展示逻辑)

```
- (void)msSplashClosed:(MSSplashAd *)splashAd{
//获取点睛view，如果有值，代表支持点睛功能
   if (self.splash.splashZoomView) {
        self.splash.splashZoomView.frame = CGRectMake(self.view.bounds.size.width-150, self.view.bounds.size.height, 130, 230);
        //此参数必传
        self.splash.splashZoomViewController = self;
        [self.view addSubview:self.splash.splashZoomView];
        [self.view bringSubviewToFront:self.splash.splashZoomView];
    }
}
```

