# iOS14支持说明

## 1. 概述

在 iOS 14 设备上，建议您在应用启动时调用 apple 提供的 AppTrackingTransparency 方案，获取用户的 IDFA 授权，以便提供更精准的广告投放和收入优化

以下将介绍iOS 14支持所需的更改。

需要在 Info.plist 中配置做如下配置并自定义文案描述:

![](../uploads/20210421/idfa-info配置.png)

```objc
<key>NSUserTrackingUsageDescription</key>
<string>该标识符将用于向您推送个性化广告</string>
```

向用户申请权限时，请调用 `requestTrackingAuthorizationWithCompletionHandler:`方法。建议您申请权限后再请求广告，以便我们准确的获得用户授权状态。

示例

```objc
#import <AdSupport/AdSupport.h>
#import <AppTrackingTransparency/AppTrackingTransparency.h>
- (void)requestIDFA {
  [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
    // 授权完成后回调
    // [self loadMSAd];
  }];
}
```

注意：

该权限只有Xcode 12+才有，需要更新Xcode 12版本来进行测试使用。

