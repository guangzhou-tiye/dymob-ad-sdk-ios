## 信息流广告(MSNativeAd)

使用说明：信息流提供两种类型广告，一种为图文类广告，另一种为视频类广告，SDK 提供信息流广告的数据绑定、点击事件的上报，用户可自行定义信息流广告展示形态与布局。

注意事项

**1、如果使用同一个广告请求对象加载多次广告，请将上次请求到的广告移除后再展示本次请求回来的广告。**

**2、加载广告前进行个性化参数设置**

#### 1.加载信息流广告

引入头文件：

```objc
#import <MSAdSDK/MSNativeAd.h>
```

您需要确认您遵循了**MSNativeAdDelegate**代理协议：

```objc
@interface MSNativeAdViewController ()<MSNativeAdDelegate>

@end
```

初始化信息流广告请求实例

```objc
self.nativeAd = [[MSNativeAd alloc]initWithCurController:self];
self.nativeAd.delegate = self;
```

加载信息流广告前务必传入广告位ID

```objc
[self.nativeAd loadAd:3 pid:pid];
```

#### 2. 展示信息流广告

信息流广告加载成功会返回一组实现了MSFeedAdMeta协议的广告物料对象

```objc
-(void)msNativeLoaded:(NSArray<MSFeedAdMeta> *)nativeAdDataArray
```

MSFeedAdMeta协议如下：

```objc
@protocol MSFeedAdMeta <NSObject>
- (MSCreativeType)metaCreativeType;//类型
- (NSString *)metaTitle;//标题
- (NSString *)metaContent;//内容
- (NSString *)metaIcon;//广告图标
- (UIImageView *)metaLogo;//平台Logo
- (CGSize)metaMainImageSize;//主图图片尺寸
- (NSArray<NSString *> *)metaImageUrls;//多图信息流
- (NSString *)metaVideoUrl;//信息流视频
- (NSTimeInterval)metaVideoDuration;//视频时长
- (NSInteger)metaTargetType;//广告交互类型(0:网页跳转,1:下载) 默认 值:0
- (NSString *)metaFromId;
#pragma mark- Action
//绑定数据到View
-(void)attachAd:(UIView *)view
       clickView:(NSArray<UIView*>*)clickViews
       presentVc:(UIViewController*)presenntVc;
//解绑数据
-(void)unAttachAd;

@end
```

获取广告元素

```objc
-(void)msNativeLoaded:(NSArray<MSFeedAdMeta> *)nativeAdDataArray{
       id<MSFeedAdMeta>data = nativeAdDataArray[0];
       //把想要的广告元素取出
       NSString *title = data.metaTitle;
       ...
}
```

在广告展示前，务必绑定事件

```objc
//取出当前将要展示的广告物料对象
id<MSFeedAdMeta>data = nativeAdDataArray[0];
//调用绑定事件方法，将参数传入
[data attachAd:self clickView:@[self] presentVc:self.adPresentVc];
```

在广告结束展示后，务必解除已绑定的事件

```objc
//取出当前结束展示的广告物料对象
id<MSFeedAdMeta>data = nativeAdDataArray[0];
[data unAttachAd];
```

#### 3. 实现MSNativeAd的Delegate

您可以实现**MSNativeAd Delegate**的方法来获取信息流广告的各种事件：

```objc
/**
 *  原生广告加载广告数据成功回调，返回为MSFeedAdData对象的数组
 */
- (void)msNativeLoaded:(NSArray<MSFeedAdMeta>*)nativeAdDataArray;
/**
 *  原生广告加载广告数据失败回调
 */
- (void)msNativeError:(NSError *)error;
/**
*  原生广告加载广告 平台数据失败回调
*/
- (void)msNativePlatformError:(MSPlatform)platform error: (NSError *)error;
/**
 *  原生广告即将展现
 */
- (void)msNativeShow:(id<MSFeedAdMeta>)feedAdData;
/**
 *  广告被点击
 */
- (void)msNativeClick:(id<MSFeedAdMeta>)feedAdData;
/**
 *  原生广告点击之后将要展示内嵌浏览器或应用内AppStore回调
 */
- (void)msNativeDetailShow;
/**
 * 原生广告点击以后，内置AppStore或是内置浏览器被关闭时回调
 */
- (void)msNativeDetailClosed;
/**
 获取当前广告的行业ID、创意ID、广告主ID
 @param adCats 投放行业ID
 @param adCids 创意ID
 @param aderIds 广告主ID
 提示：请求MS平台广告时触发该回调
 */
- (void)msNativeAdCats:(NSArray<NSString *> *)adCats
                 AdCid:(NSArray<NSString *> *)adCids
               AderIds:(NSArray<NSString *> *)aderIds;
/**
 获取ecpm参数
 详解：此回调只有在MS平台广告加载成功后才会触发
 */
- (void)msNativeEcpm:(NSString *)ecpm;
```

#### 4.获取当前展示的广告归属平台

请在此回调中获取广告归属，请参考[平台对应关系](SDK接入配置)。

```objc
- (void)msNativeLoaded:(NSArray<MSFeedAdMeta>*)nativeAdDataArray{
		NSLog(@"%d",self.nativeAd.platform);
}
```

#### 5.特殊回调说明

| 广告回调              | 回调说明                                                     |
| --------------------- | ------------------------------------------------------------ |
| msNativeError         | 在该广告位上配置的多家平台依次调度仍未加载到广告后调用，此回调代表本次请求动作已结束 |
| msNativePlatformError | 当前广告平台请求失败后调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败后触发该回调，注意此回调并不代表本次请求动作已经结束且可能存在多次回调的情况 |

