# 视频贴片广告(MSVideoAd)

使用说明：务必设置Controller，即跳转落地页需要的viewController。

**注意事项**

**1、如果使用同一个广告请求对象加载多次广告，请将上次请求到的广告移除后再展示本次请求回来的广告**

**2、加载广告前进行个性化参数设置**

#### 1.加载视频贴片广告

引入头文件：

```objc
#import <MSAdSDK/MSVideoAd.h>
```

您需要确认您遵循了**MSVideoAdDelegate**代理协议：

```objc
@interface MSVideoAdViewController ()<MSVideoAdDelegate>

@end
```

初始化视频贴片广告请求实例

```objc
MSVideoAd *videoAd = [[MSVideoAd alloc]initWithFrame:self.containView.bounds presentVc:self];
videoAd.delegate = self;                                               
```

加载视频贴片广告前务必传入广告位ID

```objc
[videoAd loadAdAndShowAd:pid];
```

#### 2. 实现MSVideoAd的Delegate

您可以实现**MSVideoAd Delegate**的方法来获取视频贴片广告的各种事件：

```objc
/**
 *  视频广告加载成功
 */
- (void)msVideoLoad:(MSVideoAd *)videoAd;
/**
 平台广告准备就绪，可以进行展示
 */
- (void)msVideoReadySuccess:(MSVideoAd *)videoAd;
/**
 *  视频广告成功展示
 */
- (void)msVideoShow:(MSVideoAd *)videoAd;

/**
 *  视频广告展示失败
 */
- (void)msVideoError:(MSVideoAd *)videoAd error:(NSError *)error;

/**
*  视频广告平台展示失败
*/
- (void)msVideoPlatformError:(MSPlatform)platform ad: (MSVideoAd *)videoAd error:(NSError *)error;

/**
 *  视频广告点击回调
 */
- (void)msVideoClick:(MSVideoAd *)videoAd;

/**
 *  视频广告将要关闭回调
 */
- (void)msVideoCompletion:(MSVideoAd *)videoAd;
/**
 获取当前广告的行业ID、创意ID、广告主ID
 @param adCats 投放行业ID
 @param adCids 创意ID
 @param aderIds 广告主ID
 提示：请求MS平台广告时触发该回调
 */
- (void)msVideoAdCats:(NSArray<NSString *> *)adCats
                AdCid:(NSArray<NSString *> *)adCids
              AderIds:(NSArray<NSString *> *)aderIds;
/**
 获取ecpm参数
 详解：此回调只有在MS平台广告加载成功后才会触发
 */
- (void)msVideoEcpm:(NSString *)ecpm;
```

#### 3.获取当前展示的广告归属平台

请在此回调中获取广告归属，请参考[平台对应关系](SDK接入配置)。

```objc
- (void)msVideoLoad:(MSVideoAd *)videoAd{
		NSLog(@"%d",videoAd.platform);
}
```

#### 4.特殊回调说明

| 广告回调             | 回调说明                                                     |
| -------------------- | ------------------------------------------------------------ |
| msVideoError         | 在该广告位上配置的多家平台依次调度仍未加载到广告后调用，此回调代表本次请求动作已结束 |
| msVideoPlatformError | 当前广告平台请求失败后调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败后触发该回调，注意此回调并不代表本次请求动作已经结束且可能存在多次回调的情况 |

