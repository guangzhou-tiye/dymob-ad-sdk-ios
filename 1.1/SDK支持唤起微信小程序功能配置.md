### SDK支持唤起微信小程序功能配置

#### 简介

SDK已支持唤起微信小程序的能力，当广告被点击后可以跳转至微信小程序指定页面。

#### 媒体接入前准备工作

- 需在微信开放平台创建移动应用
- 审核成功后，在微信开放平台获取Appid与Universal Links
- App项目中集成最新版本微信openSDK
- 在蒂烨平台将微信开放平台获取的AppID和Universal Links进行关联

#### 微信openSDK环境要求

- SDK版本: SDK1.8.6或以上
- 微信版本: 7.0.7或以上
- 系统版本: iOS12或以上

#### 集成微信openSDK

- **Cocoapods集成**

```
pod 'WechatOpenSDK', '~> 1.8.6'
```

- **手动集成** [下载链接](https://developers.weixin.qq.com/doc/oplatform/Downloads/iOS_Resource.html)

> 注意：请使用微信openSDK1.8.6版本及以上

#### 常见问题

[如何在微信开放平台创建应用](https://open.weixin.qq.com)

[如何获取微信openSDK Demo示例](https://developers.weixin.qq.com/doc/oplatform/Downloads/iOS_Resource.html)

[如何接入微信openSDK](https://developers.weixin.qq.com/doc/oplatform/Mobile_App/Access_Guide/iOS.html)

[关于微信 iOS openSDK 隐私数据使用说明](https://weixin.qq.com/cgi-bin/readtemplate?t=opensdk_agreement&lang=zh_CN)

