# 激励视频(MSRewardVideoAd)

类型说明：激励视频广告是一种全新的广告形式，用户可选择观看视频广告以换取有价物，这类广告的长度为 15-30 秒，不可跳过，且广告的结束画面会显示结束页面，引导用户进行后续动作。

**注意事项**

**1、激励视频展示时机，请在收到激励视频缓存成功回调后，调用show接口**

#### 1.加载激励视频

引入头文件：

```objc
#import <MSAdSDK/MSRewardVideoAd.h>
```

您需要确认您遵循了**MSRewardVideoAdDelegate**代理协议：

```objc
@interface MSRewardVideoViewController ()<MSRewardVideoAdDelegate>

@end
```

初始化激励视频请求实例

```objc
self.rewardAd = [[MSRewardVideoAd alloc] initWithCurController:self];
self.rewardAd.delegate = self;
```

加载激流视频广告前务必传入广告位ID

```objc
[self.rewardAd loadRewardVideoAd:pid];
```

#### 2. 展示Rewarded Video

请在收到激励视频缓存成功回调后，调用此接口

```objc
-(void)msRewardVideoCached:(MSRewardVideoAd *)msRewardVideoAd {
      [self.rewardAd showRewardVideoAd];
}
```

#### 3. 实现MSRewardVideoAd的Delegate

您可以实现**MSRewardVideoAd Delegate**的方法来获取激励视频的各种事件：

```objc
/**
 广告数据加载成功回调
 @param msRewardVideoAd MSRewardVideoAd 实例
 */
- (void)msRewardVideoLoaded:(MSRewardVideoAd *)msRewardVideoAd;
/**
 视频数据下载成功回调，已经下载过的视频会直接回调,在该回调中调用show接口
 @param msRewardVideoAd MSRewardVideoAd 实例
 */
- (void)msRewardVideoCached:(MSRewardVideoAd *)msRewardVideoAd;
/**
 激励视频渲染成功
 */
- (void)msRewardVideoRenderSuccess:(MSRewardVideoAd *)msRewardVideoAd;
/**
 激励视频渲染失败
 */
- (void)msRewardVideoRenderFail:(MSRewardVideoAd *)msRewardVideoAd error:(NSError *)error;
/**
 视频播放页即将展示回调
 @param msRewardVideoAd MSRewardVideoAd 实例
 */
- (void)msRewardVideoWillShow:(MSRewardVideoAd *)msRewardVideoAd;
/**
 视频广告曝光回调
 @param msRewardVideoAd MSRewardVideoAd 实例
 */
- (void)msRewardVideoShow:(MSRewardVideoAd *)msRewardVideoAd;
/**
 视频播放页关闭回调
 @param msRewardVideoAd MSRewardVideoAd 实例
 */
- (void)msRewardVideoClosed:(MSRewardVideoAd *)msRewardVideoAd;
/**
 视频广告信息点击回调
 @param msRewardVideoAd MSRewardVideoAd 实例
 */
- (void)msRewardVideoClicked:(MSRewardVideoAd *)msRewardVideoAd;
/**
 视频广告各种错误信息回调
 @param msRewardVideoAd MSRewardVideoAd 实例
 @param error 具体错误信息
 */
- (void)msRewardVideoError:(MSRewardVideoAd *)msRewardVideoAd error:(NSError *)error;
/**
 视频广告各种错误信息回调
 @param msRewardVideoAd MSRewardVideoAd 实例
 @param platform 广告平台
 @param error 具体错误信息
 */
- (void)msRewardVideoPlatformError:(MSRewardVideoAd *)msRewardVideoAd  platform:(MSPlatform)platform error:(NSError *)error;
/**
 视频广告播放达到激励条件回调
 @param msRewardVideoAd MSRewardVideoAd 实例
 */
- (void)msRewardVideoReward:(MSRewardVideoAd *)msRewardVideoAd;
/**
 视频广告视频播放完成
 @param msRewardVideoAd MSRewardVideoAd 实例
 */
- (void)msRewardVideoFinish:(MSRewardVideoAd *)msRewardVideoAd;
/**
 获取当前广告的行业ID、创意ID、广告主ID
 @param adCats 投放行业ID
 @param adCids 创意ID
 @param aderIds 广告主ID
 提示：请求MS平台广告时触发该回调
 */
- (void)msRewardVideoAdCats:(NSArray<NSString *> *)adCats
                      AdCid:(NSArray<NSString *> *)adCids
                    AderIds:(NSArray<NSString *> *)aderIds;
/**
 获取ecpm参数
 详解：此回调只有在MS平台广告加载成功后才会触发
 */
- (void)msRewardVideoEcpm:(NSString *)ecpm;
```

#### 4.获取当前展示的广告归属平台

请在此回调中获取广告归属，请参考[平台对应关系](SDK接入配置)。

```objc
- (void)msRewardVideoLoaded:(MSRewardVideoAd *)msRewardVideoAd{
		NSLog(@"%d",msRewardVideoAd.platform);
}
```

#### 5.特殊回调说明

| 广告回调                   | 回调说明                                                     |
| -------------------------- | ------------------------------------------------------------ |
| msRewardVideoError         | 在该广告位上配置的多家平台依次调度仍未加载到广告后调用，此回调代表本次请求动作已结束 |
| msRewardVideoPlatformError | 当前广告平台请求失败后调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败后触发该回调，注意此回调并不代表本次请求动作已经结束且可能存在多次回调的情况 |

