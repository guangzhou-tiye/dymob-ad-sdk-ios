## 新版信息流广告(MSNativeFeedAd)

新版信息流提供两种类型广告，一种为**自渲染素材广告**，其中自渲染素材广告还分为两种类型，一种为图文类广告，另一种为视频类广告，SDK 提供信息流广告的数据绑定、点击事件的上报，用户可自行定义信息流广告展示形态与布局，另一种为**模版广告**，即渲染完成的view组件。

**注意事项**

**1、如果使用同一个广告请求对象加载多次广告，请将上次请求到的广告移除后再展示本次请求回来的广告。**

**2、加载广告前进行个性化参数设置**

**3、如广告物料预处理失败即（收到msNativeFeedAdMaterialMetaReadyError回调）请将该广告物料从当前列表中移除**

#### 1.加载信息流广告

**1.1 引入头文件：**

```objc
#import <MSAdSDK/MSNativeFeedAd.h>
```

**1.2 您需要确认您遵循了MSNativeFeedAdDelegate代理协议：**

```objc
@interface MSNativeFeedAdViewController ()<MSNativeFeedAdDelegate>

@end
```

**1.3 初始化信息流广告请求实例**

```objc
self.nativeAd = [[MSNativeFeedAd alloc]initWithController:self];
self.nativeAd.delegate = self;
```

**1.4 加载信息流广告前务必传入广告位ID**

```objc
[self.nativeAd loadAd:3 pid:pid];
```

#### 2. 展示信息流广告

**2.1 信息流广告加载成功会返回一组MSNativeFeedAdModel广告物料对象**

```objc
-(void)msNativeFeedAdLoaded:(NSArray<MSNativeFeedAdModel *> *)feedAds
```

**2.2 MSNativeFeedAdModel属性介绍**

```
@interface MSNativeFeedAdModel : NSObject
/**
 详解：
 isNativeExpress为Yes时代表模版广告，请读取feedView进行广告填充
 isNativeExpress为No时代表自渲染广告，请读取adMaterialMeta进行广告填充
 feedView与adMaterialMeta目前只存在其中一个，不会同时存在
 */
@property(nonatomic,assign)BOOL isNativeExpress;//是否是模版
@property(nonatomic,strong,nullable)UIView *feedView;//广告模版
@property(nonatomic,strong,nullable)id<MSFeedAdMeta> adMaterialMeta;//广告素材
@end
```

**2.3 如何使用广告物料**

当获取到一组广告物料对象后，首先通过isNativeExpress属性判断广告类型，isNativeExpress为Yes时代表模版广告，请读取feedView进行广告填充，isNativeExpress为No时代表自渲染广告，请读取adMaterialMeta进行广告填充，**请注意MSNativeFeedAdModel中feedView和adMaterialMeta不会同时存在**。

示例如下

```
//从列表中取出广告物料对象
MSNativeFeedAdModel *obj = feedAds[index];
//判断广告类型
if (obj.isNativeExpress) {
    //取出模版广告
   UIView *feedView = obj.feedView;
}else{
   id<MSFeedAdMeta> ad = obj.adMaterialMeta;
}
```

**2.4 MSNativeFeedAdModel中feedView广告物料对象使用说明**

MSNativeFeedAdModel中feedView广告物料对象是渲染完成的view组件，取出feedView后添加至视图上即可。

**2.5 MSNativeFeedAdModel中adMaterialMeta广告物料对象使用说明**

MSNativeFeedAdModel中adMaterialMeta广告物料对象遵守了MSFeedAdMeta协议，可通过协议中的方法获取广告元素

MSFeedAdMeta协议如下：

```objc
@protocol MSFeedAdMeta <NSObject>
- (MSCreativeType)metaCreativeType;//类型
- (NSString *)metaTitle;//标题
- (NSString *)metaContent;//内容
- (NSString *)metaIcon;//广告图标
- (UIImageView *)metaLogo;//平台Logo
- (CGSize)metaMainImageSize;//主图图片尺寸
- (NSArray<NSString *> *)metaImageUrls;//多图信息流
- (NSString *)metaVideoUrl;//信息流视频
- (NSTimeInterval)metaVideoDuration;//视频时长
- (NSInteger)metaTargetType;//广告交互类型(0:网页跳转,1:下载) 默认 值:0
- (NSString *)metaFromId;
#pragma mark- Action
//绑定数据到View
-(void)attachAd:(UIView *)view
       clickView:(NSArray<UIView*>*)clickViews
       presentVc:(UIViewController*)presenntVc;
//解绑数据
-(void)unAttachAd;

@end
```

获取广告元素

```objc
MSNativeFeedAdModel *obj = feedAds[index];
id<MSFeedAdMeta>data = obj.adMaterialMeta;
//把想要的广告元素取出
NSString *title = data.metaTitle;
...
```

在广告展示前，务必绑定事件

```objc
//取出当前将要展示的广告物料对象
id<MSFeedAdMeta>data = nativeAdDataArray[0];
//调用绑定事件方法，将参数传入
[data attachAd:self clickView:@[self] presentVc:self.adPresentVc];
```

在广告结束展示后，务必解除已绑定的事件

```objc
//取出当前结束展示的广告物料对象
id<MSFeedAdMeta>data = nativeAdDataArray[0];
[data unAttachAd];
```

#### 3. 实现MSNativeFeedAd的Delegate

您可以实现**MSNativeFeedAd Delegate**的方法来获取信息流广告的各种事件：

**请注意如果广告预处理失败后即收到msNativeFeedAdMaterialMetaReadyError回调，请将该广告物料从展示列表中移除**

```objc
@protocol MSNativeFeedAdDelegate <NSObject>

@optional

/**
 *  新原生广告加载广告数据成功回调，返回为广告物料的数组
 *
 */
- (void)msNativeFeedAdLoaded:(NSArray <MSNativeFeedAdModel *> *)feedAds;
/**
 *  新原生广告加载广告数据失败回调
 */
- (void)msNativeFeedAdError:(NSError *)error;
/**
*  新原生广告加载广告 平台数据失败回调
*/
- (void)msNativeFeedAdPlatformError:(MSPlatform)platform error:(NSError *)error;
/**
 *  广告素材预处理成功回调
 */
- (void)msNativeFeedAdMaterialMetaReadySuccess:(MSNativeFeedAdModel *)feedAd;
/**
 *  广告素材预处理失败回调
 */
- (void)msNativeFeedAdMaterialMetaReadyError:(MSNativeFeedAdModel *)feedAd error:(NSError *)error;
/**
 *注意⚠️：当该广告物料是⚠️模版广告⚠️时触发此回调
 *  广告被关闭
 *  详解:广告点击关闭后回调
 */
- (void)msNativeFeedAdClosed:(MSNativeFeedAdModel *)feedAd;
/**
 *  新原生广告即将展现
 */
- (void)msNativeFeedAdShow:(MSNativeFeedAdModel *)feedAd;
/**
 *  广告被点击
 */
- (void)msNativeFeedAdClick:(MSNativeFeedAdModel *)feedAd;
/**
 *注意⚠️：当该广告物料是⚠️自渲染广告⚠️时触发此回调
 *  原生广告点击之后将要展示内嵌浏览器或应用内AppStore回调
 */
- (void)msNativeFeedAdDetailShow;
/**
 *注意⚠️：当该广告物料是⚠️自渲染广告⚠️时触发此回调
 * 新原生广告点击以后，内置AppStore或是内置浏览器被关闭时回调
 */
- (void)msNativeFeedAdDetailClosed;
/**
 获取当前广告的行业ID、创意ID、广告主ID
 @param adCats 投放行业ID
 @param adCids 创意ID
 @param aderIds 广告主ID
 提示：请求MS平台广告时触发该回调
 */
- (void)msNativeFeedAdCats:(NSArray<NSString *> *)adCats
                     AdCid:(NSArray<NSString *> *)adCids
                   AderIds:(NSArray<NSString *> *)aderIds;
/**
 获取ecpm参数
 详解：此回调只有在MS平台广告加载成功后才会触发
 */
- (void)msNativeFeedAdEcpm:(NSString *)ecpm;
```

#### 4.获取当前展示的广告归属平台

请在此回调中获取广告归属，请参考[平台对应关系](SDK接入配置)。

```objc
-(void)msNativeFeedAdLoaded:(NSArray<MSNativeFeedAdModel *> *)feedAds{
		NSLog(@"%d",self.nativeAd.platform);
}
```

#### 5.特殊回调说明

| 广告回调                    | 回调说明                                                     |
| --------------------------- | ------------------------------------------------------------ |
| msNativeFeedAdError         | 在该广告位上配置的多家平台依次调度仍未加载到广告后调用，此回调代表本次请求动作已结束 |
| msNativeFeedAdPlatformError | 当前广告平台请求失败后调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败后触发该回调，注意此回调并不代表本次请求动作已经结束且可能存在多次回调的情况 |

