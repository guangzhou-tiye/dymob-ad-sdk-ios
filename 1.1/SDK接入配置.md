# iOS集成MSMobAdSDK



### 1. 简介i

本文档介绍如何去集成iOS端的MSMobAdSDK并实现广告投放。

MSMobAdSDK demo项目地址：https://git.adxdata.com/meishu/sdk-ios-demo/-/tree/1.1

### 1.1 支持的广告类型

MSMobAdSDK支持信息流广告（MSNativeAd）、开屏广告（MSSplashAd）、模版广告（MSPrerenderAd）、插屏广告（MSInterstitialAd）、贴片广告（MSVideoAd）、激励视频广告（MSRewardVideoAd）、新版信息流（MSNativeFeedAd）。

### 1.2 MSMobAdSDK架构

![](../uploads/20210421/SDK架构设计.png)

备注：新版信息流（MSNativeFeedAd）即为信息流广告（MSNativeAd）与模版广告（MSPrerenderAd）接口合并，MSNativeFeedAd提供了获取自渲染广告物料及模版广告的能力。

如上图所示，MSMobAdSDK开放了7种广告类型接口，接入方可根据实际使用场景接入不同形式的广告。

![](../uploads/20210421/SDK支持的广告类型.png)

## 2. 配置

### 2.1 **运行环境配置**

```
SDK支持系统： iOS 9.0+
SDK编译环境： Xcode 11
SDK支持架构： x86-64,armv7,arm64,armv7s,i386
```

### 2.2 集成前准备工作

请在广告平台上创建好应用appID、广告位ID，创建第三方广告位前，请查看蒂烨SDK是否支持该类型广告，具体支持广告类型[请查阅](兼容性)。

### 2.3 导入MSMobAdSDK

在MSMobAdSDK 1.0.0.0版本以后可以选择集成想要的联盟SDK，将需要的联盟SDK导入即可。

#### 2.3.1 手动导入

**1、获取 framework 文件后直接将 {MSAdSDK.framework，MSAdSDK.bundle}文件拖入工程即可。 升级SDK必须同时更新framework和bundle文件，否则可能出现部分页面无法展示的问题。**

拖入时请按以下方式选择：

![](../uploads/20210421/拖入时请按以下方式选择.png)

拖入完请确保Copy Bundle Resources中有MSAdSDK.bundle，否则可能出现icon图片加载不出来的情况。如下图所示。

![](../uploads/20210421/CopyBundleResources.png)

**2、添加依赖库，图示如下**

```
1、Foundation.framework
2、UIKit.framework
3、WebKit.framework
4、CoreTelephony.framework
5、CoreLocation.framework
6、AdSupport.framework
7、StoreKit.framework
8、AVFoundation.framework
9、CoreMotion.framework
10、Security.framework
```

![](../uploads/20210421/添加依赖库.png)

#### 2.3.2 cocoapod引入

在podfile文件中添加:

```
# MSMobAdSDK 必须
pod 'MSMobAdSDK/MS','~>1.1.0'
# 穿山甲SDK 可选
pod 'MSMobAdSDK/CSJ','~>1.1.0'
# 百度SDK 可选
pod 'MSMobAdSDK/BD','~>1.1.0'
# 广点通SDK 可选
pod 'MSMobAdSDK/GDT','~>1.1.0'
# 京东SDK 可选
pod 'MSMobAdSDK/JD','~>1.1.0'
# 快手SDK 可选
pod 'MSMobAdSDK/KS','~>1.1.0'
```

### 2.4 iOS 14支持

从iOS 14开始，只有在获得用户明确许可的前提下，应用才可以访问用户的IDFA数据并向用户投放定向广告。请参阅详细的[iOS 14支持说明](iOS14支持说明)，以了解更多信息。

### 2.5 配置 Build Settings 和 Info.plist

1、在 Xcode中, 点击到 **Build Settings**, 搜索 **Other Linker Flags** 然后添加 **-ObjC**(这里的字母O和字母C**需要大写**), 注意 **Linker Flags** 是区分大小写的:

![配置 Build Settings](../uploads/20210421/配置BuildSettings.png)

**2、info.plist文件设置**

点击左边的targets一栏，选中当前target后边，选中正上方工具栏中的info选项， 添加 App Transport Security Settings，先点击左侧展开箭头，再点右侧加号，Allow Arbitrary Loads 选项自动加入，修改值为 YES。 SDK API 已经全部支持HTTPS，但是广告主素材存在非HTTPS情况。

![](../uploads/20210421/配置info.png)

### 2.6 导入第三方的SDK

建议使用【2.3.2 cocoapod引入】方式导入第三方SDK

| 第三方平台 | 需要导入的包                                                 | 下载地址                                                     |
| ---------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 穿山甲     | BUAdSDK.bundle<br/>BUAdSDK.framework<br/>BUFoundation.framework<br/>3.4.0.0+版本新增<br/>BUCNAuxiliary.framework | https://www.pangle.cn（请登陆穿山甲官网下载）                |
| 广点通     | libGDTMobSDK.a                                               | https://e.qq.com/dev/index.html                              |
| 百度       | BaiduMobAdSDK.framework<br/>baidumobadsdk.bundle             | https://union.baidu.com/bqt/#/                               |
| 京东       | JADYun.framework                                             | https://help-sdk-doc.jd.com/ansdkDoc/access_docs/iOS/        |
| 快手       | KSAdSDK.xcframework                                          | https://static.yximgs.com/udata/pkg/KSAdSDKTarGz/KSAdSDK-ad-3.3.9.1-225.tar.gz |

此外，MSMobAdSDK为每一个支持的第三方广告平台定义了一个枚举值（MSPlatform）：

```objc
/**
 广告平台
 */
typedef NS_ENUM(NSInteger, MSPlatform) {
    MSPlatformMS  = 0, // MS
    MSPlatformGDT = 1, // 广点通
    MSPlatformBU  = 2, // 穿山甲
    MSPlatformBD  = 3, // 百度
    MSPlatformKS  = 6, // 快手 
    MSPlatformJD  = 7  // 京东
};
```

### 2.7 MSMobAdSDK广告生命周期流程

![](../uploads/20210421/广告生命周期流程.png)

