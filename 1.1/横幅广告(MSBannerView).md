# 横幅广告(MSBannerView)

使用说明：通过设置MSBannerView代理，获取广告、展示、点击、关闭等回调。

**注意事项**

**1、如当前页面不再需要展示横幅广告或点击横幅广告上关闭按钮后，请调用dismiss方法进行移除，避免出现banner广告无法释放问题**

**2、其他banner广告个性化参数设置务必在加载广告前传入**

**3、务必传入Controller，即跳转落地页需要的viewController**

#### 1.加载横幅广告

引入头文件：

```objc
#import <MSAdSDK/MSBannerView.h>
```

您需要确认您遵循了**MSBannerViewDelegate**代理协议：

```objc
@interface MSBannerViewController ()<MSBannerViewDelegate>

@end
```

初始化横幅广告请求实例

```objc
-(MSBannerView *)bannerView{
    if (!_bannerView) {
        _bannerView = [[MSBannerView alloc]initWithFrame:self.containView.bounds curController:self];
    }
    return _bannerView;
}
```

加载横幅广告前务必传入广告位ID

```objc
[self.bannerView loadAdAndShow:self pid:pid];
```

#### 2. 展示横幅广告

可在收到msBannerAdReadySuccess回调后将MSBannerView实例对象添加在指定视图上

```
-(void)msBannerAdReadySuccess:(MSBannerView *)msBannerAd{
    if (!self.bannerView.superview) {
        [self.containView addSubview:_bannerView];
    }
}
```

#### 3. MSBannerView生命周期回调

您可以实现**MSBannerView Delegate**的方法来获取横幅广告的各种事件：

```objc
/**
 *  请求广告条数据成功后调用
 *  详解:当接收服务器返回的广告数据成功后调用该函数
 */
- (void)msBannerLoaded:(MSBannerView *)msBannerAd;
/**
 *  请求广告条数据失败后调用
 *  详解:当接收服务器返回的广告数据失败后调用该函数
 */
- (void)msBannerError:(MSBannerView *)msBannerAd error:(NSError *)error;
/**
 *  请求平台广告条数据失败后调用
 *  详解:当接收服务器返回的广告数据失败后调用该函数
 */
- (void)msBannerPlatformError:(MSPlatform)platform
                     bannerAd:(MSBannerView *)msBannerAd
                        error:(NSError *)error;
/**
 平台广告准备就绪，可以进行展示
 */
- (void)msBannerAdReadySuccess:(MSBannerView *)msBannerAd;
/**
 广告渲染成功
 */
- (void)msBannerAdRenderSuccess:(MSBannerView *)msBannerAd;
/**
 广告渲染失败
 */
- (void)msBannerAdRenderFail:(MSBannerView *)msBannerAd error:(NSError *)error;
/**
 *  banner广告曝光
 */
- (void)msBannerShow:(MSBannerView *)msBannerAd;
/**
 *  banner条点击回调
 */
- (void)msBannerClicked:(MSBannerView *)msBannerAd;
/**
 *  banner条被用户关闭时调用
 *  详解:用户有可能点击关闭按钮从而把广告条关闭
 */
- (void)msBannerClosed:(MSBannerView *)msBannerAd;
/**
 *  banner广告点击以后弹出全屏广告页完毕
 */
- (void)msBannerDetailShow:(MSBannerView *)msBannerAd;
/**
 *  全屏广告页已经被关闭
 */
- (void)msBannerDetailClosed:(MSBannerView *)msBannerAd;
/**
 获取当前广告的行业ID、创意ID、广告主ID
 @param adCats 投放行业ID
 @param adCids 创意ID
 @param aderIds 广告主ID
 提示：请求MS平台广告时触发该回调
 */
- (void)msBannerAdCats:(NSArray<NSString *> *)adCats
                 AdCid:(NSArray<NSString *> *)adCids
               AderIds:(NSArray<NSString *> *)aderIds;
/**
 获取ecpm参数
 详解：此回调只有在MS平台广告加载成功后才会触发
 */
- (void)msBannerEcpm:(NSString *)ecpm;
```

#### 4.获取当前展示的广告归属平台

请在此回调中获取广告归属，请参考[平台对应关系](SDK接入配置)。

```objc
-(void)msBannerLoaded:(MSBannerView *)msBannerAd{
		NSLog(@"%d",msBannerAd.platform);
}
```

#### 5.移除横幅广告

如当前页面不再需要展示横幅广告或点击横幅广告关闭按钮后，移除方法如下

```
[self.bannerView dismiss];
_bannerView = nil;
```

#### 6.特殊回调说明

| 广告回调                | 回调说明                                                     |
| ----------------------- | ------------------------------------------------------------ |
| msBannerError           | 在该广告位上配置的多家平台依次调度仍未加载到广告后调用，此回调代表本次请求动作已结束 |
| msBannerPlatformError   | 当前广告平台请求失败后调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败后触发该回调，注意此回调并不代表本次请求动作已经结束且可能存在多次回调的情况 |
| msBannerAdRenderSuccess | 该回调如果第三方sdk支持轮播会调用多次                        |
| msBannerAdRenderFail    | 该回调如果第三方sdk支持轮播可能会调用多次                    |

