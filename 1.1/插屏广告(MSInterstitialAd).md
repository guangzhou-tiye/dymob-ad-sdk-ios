# 插屏广告(MSInterstitialAd)

使用说明：通过设置MSInterstitialAd代理，获取广告、展示、点击、关闭等回调。务必设置Controller，即跳转落地页需要的viewController。

#### 1.加载插屏广告

引入头文件：

```objective-c
#import <MSAdSDK/MSInterstitialAd.h>
```

您需要确认您遵循了**MSInterstitialDelegate**代理协议：

```objective-c
@interface MSInterstitialViewController ()<MSInterstitialDelegate>

@end
```

初始化插屏广告请求实例

```objective-c
self.interstitialAd = [[MSInterstitialAd alloc]initWithCurController:self];
self.interstitialAd.delegate = self;
```

加载插屏广告前务必传入广告位ID

```objective-c
[self.interstitialAd loadAd:pid];
```

#### 2. 展示插屏广告

为了保证广告正常展示，建议在收到平台广告展示准备就绪回调后，调用show接口

```objective-c
- (void)msInterstitialAdReadySuccess:(MSInterstitialAd *)msInterstitialAd{
    [self.interstitialAd showAd];
}
```

#### 3. 实现MSInterstitialAd的Delegate

您可以实现**MSInterstitialAd Delegate**的方法来获取插屏广告的各种事件：

```objective-c
/**
 *  广告预加载成功回调
 *  详解:当接收服务器返回的广告数据成功且预加载后调用该函数
 */
- (void)msInterstitialLoaded:(MSInterstitialAd *)msInterstitialAd;
/**
 *  广告预加载失败回调
 *  详解:当接收服务器返回的广告数据失败后调用该函数
 */
- (void)msInterstitialError:(MSInterstitialAd *)msInterstitialAd
                      error:(NSError *)error;
/**
*  平台广告失败回调
*
*/
- (void)msInterstitialPlatformError:(MSPlatform)platform ad: (MSInterstitialAd *)msInterstitialAd error:(NSError *)error;
/**
 平台广告展示准备就绪，可以进行展示
 */
- (void)msInterstitialAdReadySuccess:(MSInterstitialAd *)msInterstitialAd;
/**
 *  插屏广告视图展示成功回调
 *  详解: 插屏广告展示成功回调该函数
 */
- (void)msInterstitialShow:(MSInterstitialAd *)msInterstitialAd;
/**
   平台广告展示失败
   详解：可能广告素材异常或三方异常导致无法广告曝光
 */
- (void)msInterstitialAdShowFail:(MSInterstitialAd *)msInterstitialAd error:(NSError *)error;
/**
 *  插屏广告展示结束回调
 *  详解: 插屏广告展示结束回调该函数
 */
- (void)msInterstitialClosed:(MSInterstitialAd *)msInterstitialAd;
/**
 *  插屏广告点击回调
 */
- (void)msInterstitialClicked:(MSInterstitialAd *)msInterstitialAd;
/**
 *  全屏广告页被关闭
 */
- (void)msInterstitialDetailClosed:(MSInterstitialAd *)msInterstitialAd;
/**
 获取当前广告的行业ID、创意ID、广告主ID
 @param adCats 投放行业ID
 @param adCids 创意ID
 @param aderIds 广告主ID
 提示：请求MS平台广告时触发该回调
 */
- (void)msInterstitialAdCats:(NSArray<NSString *> *)adCats
                       AdCid:(NSArray<NSString *> *)adCids
                     AderIds:(NSArray<NSString *> *)aderIds;
/**
 获取ecpm参数
 详解：此回调只有在MS平台广告加载成功后才会触发
 */
- (void)msInterstitialEcpm:(NSString *)ecpm;
```

#### 4.获取当前展示的广告归属平台

请在此回调中获取广告归属，请参考[平台对应关系](SDK接入配置)。

```objc
- (void)msInterstitialLoaded:(MSInterstitialAd *)msInterstitialAd{
		NSLog(@"%d",msInterstitialAd.platform);
}
```

#### 5.特殊回调说明

| 广告回调                    | 回调说明                                                     |
| --------------------------- | ------------------------------------------------------------ |
| msInterstitialError         | 在该广告位上配置的多家平台依次调度仍未加载到广告后调用，此回调代表本次请求动作已结束 |
| msInterstitialPlatformError | 当前广告平台请求失败后调用，为保证填充，该广告位可能配置多家平台，当前平台请求失败后触发该回调，注意此回调并不代表本次请求动作已经结束且可能存在多次回调的情况 |

