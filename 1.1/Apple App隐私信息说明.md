# Apple App隐私信息说明

从2020年12月8日开始，Apple App Store要求App更新和新App上架都必须披露[App隐私信息](https://developer.apple.com/app-store/app-privacy-details/)，即需要开发者提供App以及App中所有第三方SDK使用的隐私信息。

本文档将说明MSMobAdSDK及聚合的第三方广告平台SDK收集的隐私信息。

###  1. MSMobAdSDK收集的数据类型

| 数据类型                                                     | MSMobAdSDK收集情况           | 备注                                                         |
| ------------------------------------------------------------ | ---------------------------- | ------------------------------------------------------------ |
| **联系信息** Contact Info <br/>姓名 Name <br/>电子邮件地址 Email Address <br/>电话号码 Phone Number <br/>实际地址 Physical Address <br/>其他用户联系信息 Other User Contact Info | 不收集                       |                                                              |
| **健康与健身** Health and Fitness <br/>健康 Health <br/>健身 Fitness | 不收集                       |                                                              |
| **财务信息** Financial Info <br/>付款信息 Payment Info <br/>信用信息 Credit Info <br/>其他财务信息 Other Financial Info | 不收集                       |                                                              |
| **位置 Location**                                            | 可选收集                     | MSMobAdSDK会获取地理位置信息用于广告投放与反作弊。 <br/>应用被用户授予地理位置权限时，MSMobAdSDK会获取地理位置信息，用于广告定向与反作弊；<br/>当应用不获取地理位置权限时，MSMobAdSDK不会主动获取地理位置权限及地理位置信息。 |
| **敏感信息 Sensitive Info**                                  | 不收集                       |                                                              |
| **联系人 Contacts**                                          | 不收集                       |                                                              |
| **用户内容 User Content** <br/>电子邮件或短信内容 Emails or Text Messages <br/>照片或视频 Photos or Videos <br/>音频数据 Audio Data <br/>游戏内容 Gameplay Content <br/>客户支持 Customer Support <br/>其他用户内容 Other User Content | 不收集                       |                                                              |
| **浏览历史记录 Browsing History**                            | 不收集                       |                                                              |
| **搜索历史记录 Search History**                              | 不收集                       |                                                              |
| **标识符 Identifiers** <br/>用户ID User ID <br/>设备ID Device ID | 用户ID不收集 <br/>设备ID收集 | **设备ID** <br/>当应用被用户授予广告追踪权限时，MSMobAdSDK将获取idfa用于广告归因与追踪。 |
| **购买项目 Purchases** <br/>购物历史 Purchase History        | 不收集                       |                                                              |
| **使用数据 Usage Data** <br/>产品交互 Product Interaction <br/>广告数据 Advertising Data <br/>其他使用数据 Other Usage Data | 不收集 <br/>收集 <br/>不收集 | MSMobAdSDK将统计下列广告数据，以用于广告主统计投放结果。展示 、点击 、转化 |
| **诊断 Diagnostics** <br/>崩溃数据 Crash Data <br/>性能数据 Performance Data <br/>其他诊断数据 Other Diagnostic Data | 不收集                       |                                                              |
| **其他数据 Other Data** <br/>其他数据类型 Other Data Types   | 收集                         | 技术上我们还会收集一些设备信息（例如，设备型号、操作系统及版本、时区、网络类型等） |

### 2.第三方广告平台隐私政策

以下是第三方广告平台的隐私政策说明，以便开发者了解更多。

| 第三方平台 | 参考地址                                                     |
| ---------- | ------------------------------------------------------------ |
| 广点通     | https://www.tencent.com/zh-cn/privacy-policy.html            |
| 穿山甲     | https://www.pangleglobal.com/privacy                         |
| 百度       | https://union.baidu.com/bqt/#/policies                       |
| 京东       | https://help-sdk-doc.jd.com/ansdkDoc/access_docs/iOS/广告接入/隐私数据获取说明.html |

