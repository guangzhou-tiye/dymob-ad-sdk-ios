# MSMobAdSDK初始化说明

## 1.集成第三方SDK版本要求

| 广告平台 | 最低版本要求       |
| -------- | ------------------ |
| 广点通   | 4.11.10 版本及以上 |
| 穿山甲   | 3.2.0.0 版本及以上 |
| 百度     | 4.71 版本及以上    |
| 京东     | 1.2.0 版本及以上   |
| 快手     | 3.3.9.1版本及以上  |

## 2. 集成第三方SDK版本检测

通过MSAdSDK类提供的以下接口获取第三方sdk版本号，如果获取不到对应的版本号则可能第三方sdk未集成或当前集成的版本过低。

```objective-c
//获取百度版本号
[MSAdSDK platformVersion:MSPlatformBD];
//获取穿山甲版本号
[MSAdSDK platformVersion:MSPlatformBU];
//获取广点通版本号
[MSAdSDK platformVersion:MSPlatformGDT];
//获取京东版本号
[MSAdSDK platformVersion:MSPlatformJD];
//获取快手版本号
[MSAdSDK platformVersion:MSPlatformKS];
```

### 3.调试log信息输出

```objective-c
//将log开关打开
[MSAdSDK setLogLevel:MSLogAll];
```

### 4.初始化MSMobAdSDK

##### 4.1 MSAdSDK类是整个 SDK 设置的入口和接口，可以设置 SDK 的一些全局信息，提供类方法获取设置结果，接口说明如下。

```objective-c
//蒂烨SDK入口 设置蒂烨AppID
+ (void)setAppId:(NSString *)appId;
//打开测试模式
+ (void)setTestMode:(BOOL)isTestMode;
//日志打印
+(void)setLogLevel:(MSLogLevel)level;
//蒂烨VersionName
+ (NSString *)getVersionName;
//蒂烨VersionCode
+ (NSInteger)getVersionCode;
//获取平台版本号
+ (NSString*)platformVersion:(MSPlatform)platform;
//设置支持的通信协议
+ (void)setSecure:(MSSecure)secure
//是否支持https
+ (void)setHttpsSupport:(BOOL)isSupportHttps;
```

##### 4.2 初始化设置

在AppDelegate中设置如下：

```objective-c
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    [MSAdSDK setTestMode:NO]; // 切换测试服务器，默认为生产服务器,切换环境时务必在设置appid前设置。
    [MSAdSDK setAppId:appid];  //设置AppID
    [MSAdSDK setLogLevel:MSLogNone];
}
```

在 main.m 中，引入MSApplication，代码如下:

```objective-c
#import "AppDelegate.h"
#import <MSAdSDK/MSAdSDK.h>
int main(int argc, char * argv[]) { 
@autoreleasepool {
return UIApplicationMain(argc, argv, NSStringFromClass([MSApplication class]), NSStringFromClass([AppDelegate class]));
}}
```

